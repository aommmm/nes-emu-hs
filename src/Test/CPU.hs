{-# LANGUAGE ScopedTypeVariables, TemplateHaskell #-}

module Test.CPU
    (
        testAll
    ) where

import Data.Word
import Data.Array.ST
import Data.STRef
import Data.Bits
import Control.Monad.ST
import Control.Monad.State
import Control.Monad.Trans.Reader
import Control.Monad.ST.Trans (STT, newSTArray, readSTArray, writeSTArray, runSTT)
import Debug.Trace
import Memory
import Instruction
import Types

import Test.QuickCheck.All
import CPU

tests = all id $  [
        enableNthFlag' 0 0 == 1,
        enableNthFlag' 1 0 == 2,
        enableNthFlag' 7 0 == 128,
        enableNthFlag' 7 0 == 128
    ]

prop_EnableMultipleTimesDoesNothing :: Word8 -> Bool
prop_EnableMultipleTimesDoesNothing byte =
    all id [prop n | n <- [0..7]]
    where prop n = (enableNthFlag' n byte) == (enableNthFlag' n) (enableNthFlag' n byte)

prop_DisableMultipleTimesDoesNothing :: Word8 -> Bool
prop_DisableMultipleTimesDoesNothing byte =
    all id [prop n | n <- [0..7]]
    where prop n = (disableNthFlag' n byte) == (disableNthFlag' n) (disableNthFlag' n byte)

prop_ToggleInverse :: Word8 -> Bool
prop_ToggleInverse byte =
    all id [prop n | n <- [0..7]]
    where prop n = byte == (toggleNthFlag' n) (toggleNthFlag' n byte)


return [] -- hack needed for quickcheck
testAll = $quickCheckAll
-- TODO: implement stack operations