
module Memory
    ( Memory,
      MemoryValue,
      MemoryAddress,
      MemoryZeroPageAddress,
      MemoryOffset,
      runTestMemory,
      readMemory',
      writeMemory',
      readMemory,
      writeMemory,
      initialMemory
    ) where

import Data.Word
import Data.Array.ST
import Control.Monad.ST
import Control.Monad.State
import Control.Monad.Trans.Reader
import Control.Monad.ST.Trans (STT, newSTArray, readSTArray, writeSTArray, runSTT)
import Debug.Trace

import Types


-- Test-specific types (easier to test with Int values than Word8)
type TestMemory s = STArray s MemoryAddress Int
type TestResult = (Int, Int)
type TestResults = [TestResult]

----------------------------------------------
-- Implementation

initialMemory :: Monad m => STT s m (Memory s)
initialMemory = newSTArray (0, 0xFFFF) 123



readMemory :: MemoryAddress -> Emulator s s2 MemoryValue
readMemory addr = do
  memoryRef <- asks memory
  lift $ readMemory' memoryRef addr

writeMemory :: MemoryAddress -> MemoryValue -> Emulator s s2 ()
writeMemory = undefined

-- Details


mirroredIndex start size i = 
    let result = (i `mod` size) + start
    in trace ("hello" ++ show start ++ ", " ++ show size ++ ", " ++ show i ++ "=" ++ show result ) $ result

readMemory' :: (MArray a e m) => a MemoryAddress e -> MemoryAddress -> m e 
readMemory' arr i | i < 0x800   = readArray arr i
                 | i < 0x2000  = readArray arr $ mirroredIndex 0x800 0x800 i
                 | i < 0x4000  = readArray arr $ mirroredIndex 0x1000 0x8 i
                 | i <= 0xFFFF = readArray arr $ i - 0x4000 + 0x1008
                 | otherwise   = error $ "index out of bounds"

writeMemory' :: (MArray a e m) => a MemoryAddress e -> MemoryAddress -> e -> m () 
writeMemory' arr i | i < 0x800   = writeArray arr i
                  | i < 0x2000  = writeArray arr (mirroredIndex 0x800 0x800 i) 
                  | i < 0x4000  = writeArray arr (mirroredIndex 0x1000 0x8 i) 
                  | i <= 0xFFFF = writeArray arr (i - 0x4000 + 0x1008) 
                  | otherwise   = error $ "index out of bounds"

----------------------------------------------
-- Tests

runTestMemory :: IO ()
runTestMemory = do
    putStrLn "testing memory read/write"
    let x = runST testMemory
    let testResults = map checkTestResult x
    print testResults
    return ()
    where
        checkTestResult :: TestResult -> String 
        checkTestResult (actual, expected) | actual == expected = "pass"
        checkTestResult (actual, expected) | otherwise = "fail: "++show actual ++ "!==" ++ show expected

testMemory :: ST s TestResults
testMemory = do 
               arr <- newListArray (0,0xFFFF) [0..] :: ST s (TestMemory s)
               let testCases = [(0, 0), (100, 100), (0x800-1, 0x800-1), (0x800, 0x800), (0x1800, 0x800), (0x1800-1, 0x1000-1), (0x2000-1, 0x1000-1)]
               let inputs = map fst testCases
               let expectedResults = map snd testCases
               results <- mapM (readMemory' arr) inputs
               let as' = results `zip` expectedResults
               writeMemory' arr 0 123
               b <- readMemory' arr 0
               writeMemory' arr 0x800 124
               c <- readMemory' arr 0x800
               d <- readMemory' arr 0x1800
               writeMemory' arr 0x2008 125
               e <- readMemory' arr 0x2000
               f <- readMemory' arr (0x4000-8)

               let bs = [(b,123), (c,124),(d,124), (e,125), (f,125)]
               return $ concat [as', bs]

