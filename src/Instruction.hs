{-#LANGUAGE GADTs, StandaloneDeriving #-}

module Instruction
    ( runTestInstruction
    ) where

import Data.Word
import Data.Array.ST
import Control.Monad.ST
import Debug.Trace
import Memory
import CPU
import Types


----------------------------------------------
-- Tests
runTestInstruction :: IO ()
runTestInstruction = do
    putStrLn "hello"
    let instruction = AddWithCarry (Immediate 0xFF)
    print instruction
    return ()


----------------------------------------------
-- Instructions
class Instruction i where
    runInstruction :: i -> Emulator s s2 ()

data AddWithCarry a where AddWithCarry :: (GetAddressingMode a) => a -> AddWithCarry a

data LDA a where LDA :: (GetAddressingMode a) => a -> LDA a
data LDX a where LDX :: (GetAddressingMode a) => a -> LDX a
data LDY a where LDY :: (GetAddressingMode a) => a -> LDY a
data STA a where STA :: (GetAddressingMode a) => a -> STA a
data STX a where STX :: (GetAddressingMode a) => a -> STX a
data STY a where STY :: (GetAddressingMode a) => a -> STY a

instance Instruction (AddWithCarry a) where
    runInstruction (AddWithCarry addressingMode) = do
        value <- getValue addressingMode
        return ()

instance Instruction (LDA a) where
    runInstruction (LDA addressingMode) = do
        value <- getValue addressingMode
        writeA value

instance Instruction (LDX a) where
    runInstruction (LDX addressingMode) = do
        value <- getValue addressingMode
        writeX value

instance Instruction (LDY a) where
    runInstruction (LDY addressingMode) = do
        value <- getValue addressingMode
        writeY value

instance Instruction (STA a) where
    runInstruction (STA addressingMode) = do
        a <- readA
        setValue addressingMode a

instance Instruction (STX a) where
    runInstruction (STX addressingMode) = do
        x <- readX
        setValue addressingMode x

instance Instruction (STY a) where
    runInstruction (STY addressingMode) = do
        y <- readY
        setValue addressingMode y

deriving instance Show i => Show (AddWithCarry i)


----------------------------------------------
-- Addressing modes

class GetAddressingMode a where
    getAddress :: a -> Emulator s s2 MemoryAddress
    getValue :: a -> Emulator s s2 MemoryValue
    getValue addressingMode = do
        address <- getAddress addressingMode
        readMemory address

class (GetAddressingMode a) => SetAddressingMode a where
    setValue :: a -> MemoryValue -> Emulator s s2 ()
    setValue addressingMode value = do
        address <- getAddress addressingMode
        writeMemory address value

data Implicit
newtype Immediate = Immediate MemoryValue deriving Show
newtype ZeroPage = ZeroPage MemoryZeroPageAddress deriving Show
newtype ZeroPageX = ZeroPageX MemoryZeroPageAddress deriving Show
newtype ZeroPageY = ZeroPageY MemoryZeroPageAddress deriving Show
newtype Relative = Relative MemoryOffset deriving Show
newtype Absolute = Absolute MemoryAddress deriving Show
newtype AbsoluteX = AbsoluteX MemoryAddress deriving Show
newtype AbsoluteY = AbsoluteY MemoryAddress deriving Show
newtype Indirect = Indirect MemoryAddress deriving Show
newtype IndexedIndirect = IndexedIndirect MemoryZeroPageAddress deriving Show
newtype IndirectIndexed = IndirectIndexed MemoryZeroPageAddress deriving Show

instance GetAddressingMode Immediate where
    getAddress = undefined
    getValue (Immediate value) = return value

instance GetAddressingMode ZeroPage where
    getAddress (ZeroPage address) = return $ fromIntegral address
instance SetAddressingMode ZeroPage

-- TODO: wraparound?
instance GetAddressingMode ZeroPageX where
    getAddress (ZeroPageX address) = do
        x <- readX
        let address' = fromIntegral $ address + x
        return address'
instance SetAddressingMode ZeroPageX

instance GetAddressingMode ZeroPageY where
    getAddress (ZeroPageY address) = do
        y <- readY
        let address' = fromIntegral $ address + y
        return address'
instance SetAddressingMode ZeroPageY

instance GetAddressingMode Relative where
    getAddress (Relative offset) = do
        pc <- readPC
        let address = fromIntegral $ pc + (fromIntegral offset)
        return address

instance GetAddressingMode Absolute where
    getAddress (Absolute address) = return address
instance SetAddressingMode Absolute

instance GetAddressingMode AbsoluteX where
    getAddress (AbsoluteX address) = do
        x <- readX
        let address' = fromIntegral $ address + (fromIntegral x)
        return address'
instance SetAddressingMode AbsoluteX

instance GetAddressingMode AbsoluteY where
    getAddress (AbsoluteY address) = do
        y <- readY
        let address' = fromIntegral $ address + (fromIntegral y)
        return address'
instance SetAddressingMode AbsoluteY

instance GetAddressingMode Indirect where
    getAddress (Indirect lsbAddressAddress) = do
        address <- readAddressFromMemory lsbAddressAddress
        return address

-- TODO: do wrap-around of address
instance GetAddressingMode IndexedIndirect where
    getAddress (IndexedIndirect lsbAddressAddress) = do
        address <- readZeroPageAddressFromMemory lsbAddressAddress
        x <- readX
        let address' = fromIntegral $ address + (fromIntegral x)
        return address'
instance SetAddressingMode IndexedIndirect

-- TODO: do wrap-around of address (?)
instance GetAddressingMode IndirectIndexed where
    getAddress (IndirectIndexed lsbAddressAddress) = do
        address <- readZeroPageAddressFromMemory lsbAddressAddress
        y <- readY
        let address' = fromIntegral $ address + (fromIntegral y)
        return address'
instance SetAddressingMode IndirectIndexed

----------------------------------------------
-- Utils

-- TODO: wraparound?
readZeroPageAddressFromMemory :: MemoryZeroPageAddress -> Emulator s s2 MemoryAddress
readZeroPageAddressFromMemory lsbAddressAddress = do
    lsbAddress <- readMemory $ fromIntegral lsbAddressAddress
    msbAddress <- readMemory $ fromIntegral $ lsbAddressAddress + 1
    let address = (fromIntegral msbAddress) * 0x100 + (fromIntegral lsbAddress)
    return address

readAddressFromMemory :: MemoryAddress -> Emulator s s2 MemoryAddress
readAddressFromMemory lsbAddressAddress = do
    lsbAddress <- readMemory lsbAddressAddress
    msbAddress <- readMemory $ lsbAddressAddress + 1
    let address = (fromIntegral msbAddress) * 0x100 + (fromIntegral lsbAddress)
    return address

