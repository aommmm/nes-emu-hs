{-#LANGUAGE StandaloneDeriving #-}

module General
    ( 
    ) where

import Data.Word
import Data.Array.ST
import Data.STRef
import Control.Monad.ST
import Control.Monad.State
import Control.Monad.Trans.Reader
import Control.Monad.ST.Trans (STT, newSTArray, readSTArray, writeSTArray, runSTT)
import Debug.Trace
import Memory
import Instruction
import Types
import CPU

------------------------------------------------------------------------
-- Memory
-- TODO: move to Memory module if successful

-- STArray here, is it from STT or ST? why does this work? 
-- type Memory s = STArray s MemoryAddress MemoryValue

readSomeMemory :: Emulator s s2 MemoryValue
readSomeMemory = do
    memoryRef <- asks memory
    lift $ readSTArray memoryRef 23


------------------------------------------------------------------------
-- Runners

setInitialRegisters :: Emulator s s2 String -> Emulator s s2 String
setInitialRegisters p = do
    x <- lift $ lift $ initialRegisters
    arr <- lift $ initialMemory
    let state = EmulatorState {
        registers = x,
        memory = arr
    }
    y <- local (const state) p
    return y

-- Run the Reader part of Emulator
-- (right now, have to pass "undefined" as initial value and then use setInitialRegisters immediately)
runEmulator :: Emulator s s2 String -> AllMySTs s s2 String
runEmulator p = runWithUndefined (setInitialRegisters p)
    where runWithUndefined :: Emulator s s2 a -> AllMySTs s s2 a
          runWithUndefined m = runReaderT m undefined

------------------------------------------------------------------------
-- Example program

myProgram :: Emulator s s2 String
myProgram = do
    writeX 26
    x <- readX
    writeX 101
    x2 <- readX
    otherMemoryEl <- readSomeMemory
    return $ "helo" ++ show x ++ ", " ++ show x2 ++ ", " ++ show otherMemoryEl

------------------------------------------------------------------------
-- Main

run :: IO ()
run = do
    putStrLn "hello"
    -- level 2
    let ranReader = runEmulator myProgram  -- Pass '5' in as read-only state. Rand StdGen [String]
    let ranSTT = runSTT ranReader
    let ranST = runST ranSTT
    putStrLn ranST
    
    --let y = runST thing2
    -- print y
    return ()

