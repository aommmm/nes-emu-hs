module Types
    (
        Registers(..),
        Emulator(..),
        EmulatorState(..),
        AllMySTs,
        MemoryAddress,
        MemoryZeroPageAddress,
        MemoryOffset,
        MemoryValue,
        Memory,
        isNegative
    ) where

import Data.Bits
import Data.Word
import Data.Array.ST
import Data.STRef
import Control.Monad.ST
import Control.Monad.State
import Control.Monad.Trans.Reader
import Control.Monad.ST.Trans (STT)
import Debug.Trace


-- Memory

type MemoryAddress = Word16
type MemoryZeroPageAddress = Word8
type MemoryOffset = Word8

type MemoryValue = Word8

type Memory s = STArray s MemoryAddress MemoryValue

-- CPU 

-- TODO: use STT s Identity instead of ST, for consistency
data Registers s = Registers {
    xRegister :: STRef s Word8,
    yRegister :: STRef s Word8,
    aRegister :: STRef s Word8,
    pcRegister :: STRef s Word16,
    pRegister :: STRef s Word8,
    sRegister :: STRef s Word16
}

-- General

data EmulatorState s s2 = EmulatorState {
    registers :: Registers s,
    memory :: Memory s2
}

type Emulator s s2 a = ReaderT (EmulatorState s s2) (AllMySTs s s2) a
type AllMySTs s s2 = STT s2 (ST s)

-- Utils
isNegative :: MemoryValue -> Bool
isNegative b = testBit b 7
