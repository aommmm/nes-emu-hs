
module CPU
    (
        initialRegisters,
        readX,
        readY,
        readA,
        readPC,
        readP,
        writeX,
        writeY,
        writeA,
        writePC,
        writeP,
        enableNthFlag',
        enableNthFlag,
        disableNthFlag',
        disableNthFlag,
        toggleNthFlag',
        toggleNthFlag,
        enableCarryFlag,
        enableZeroFlag,
        enableInterruptDisable,
        enableDecimalModeFlag,
        enableBreakCommand,
        enableOverflowFlag,
        enableNegativeFlag,
        pushStack,
        popStack
    ) where

import Data.Word
import Data.Array.ST
import Data.STRef
import Data.Bits
import Control.Monad.ST
import Control.Monad.State
import Control.Monad.Trans.Reader
import Control.Monad.ST.Trans (STT, newSTArray, readSTArray, writeSTArray, runSTT)
import Debug.Trace
import Memory
-- import Instruction
import Types

------------------------------------------------------------------------
-- CPU register


-- TODO: read/write specific P bits

initialRegisters :: ST s (Registers s)
initialRegisters = do
    xRef <- newSTRef 0 
    yRef <- newSTRef 0
    aRef <- newSTRef 0
    pcRef <- newSTRef 0
    pRef <- newSTRef 0
    sRef <- newSTRef 0x01FF
    return $ Registers {
        xRegister = xRef,
        yRegister = yRef,
        aRegister = aRef,
        pcRegister = pcRef,
        pRegister = pRef,
        sRegister = sRef
    }

readX :: Emulator s s2 Word8
readX = do
    xRef <- asks (xRegister.registers)
    lift $ lift $ readSTRef xRef

writeX :: Word8 -> Emulator s s2 ()
writeX x = do
    xRef <- asks (xRegister.registers)
    lift $ lift $ writeSTRef xRef x
    when (x == 0) enableZeroFlag -- TODO: keep this here on in instructions?
    when (isNegative x) enableNegativeFlag -- TODO: keep this here on in instructions?

readY :: Emulator s s2 Word8
readY = do
    yRef <- asks (yRegister.registers)
    lift $ lift $ readSTRef yRef

writeY :: Word8 -> Emulator s s2 ()
writeY y = do
    yRef <- asks (yRegister.registers)
    lift $ lift $ writeSTRef yRef y
    when (y == 0) enableZeroFlag -- TODO: keep this here on in instructions?
    when (isNegative y) enableNegativeFlag -- TODO: keep this here on in instructions?

readA :: Emulator s s2 Word8
readA = do
    aRef <- asks (aRegister.registers)
    lift $ lift $ readSTRef aRef

writeA :: Word8 -> Emulator s s2 ()
writeA a = do
    aRef <- asks (aRegister.registers)
    lift $ lift $ writeSTRef aRef a
    when (a == 0) enableZeroFlag -- TODO: keep this here on in instructions?
    when (isNegative a) enableNegativeFlag -- TODO: keep this here on in instructions?

readPC :: Emulator s s2 Word16
readPC = do
    pcRef <- asks (pcRegister.registers)
    lift $ lift $ readSTRef pcRef

writePC :: Word16 -> Emulator s s2 ()
writePC pc = do
    pcRef <- asks (pcRegister.registers)
    lift $ lift $ writeSTRef pcRef pc

readP :: Emulator s s2 Word8
readP = do
    pRef <- asks (pRegister.registers)
    lift $ lift $ readSTRef pRef

writeP :: Word8 -> Emulator s s2 ()
writeP p = do
    pRef <- asks (pRegister.registers)
    lift $ lift $ writeSTRef pRef p

readS :: Emulator s s2 Word16
readS = do
    sRef <- asks (sRegister.registers)
    lift $ lift $ readSTRef sRef

writeS :: Word16 -> Emulator s s2 ()
writeS s = do
    sRef <- asks (sRegister.registers)
    lift $ lift $ writeSTRef sRef s

incS :: Emulator s s2 ()
incS = do
    s <- readS
    writeS $ s + 1

decS :: Emulator s s2 ()
decS = do
    s <- readS
    writeS $ s - 1

modifyP :: (Word8 -> Word8) -> Emulator s s2 ()
modifyP f = do
    p <- readP
    let p' = f p
    writeP p'

enableNthFlag :: Int -> Emulator s s2 ()
enableNthFlag n = modifyP (enableNthFlag' n)

enableNthFlag' :: Int -> Word8 -> Word8
enableNthFlag' n p = p .|. (shift 1 n)

disableNthFlag :: Int -> Emulator s s2 ()
disableNthFlag n = modifyP (disableNthFlag' n)

disableNthFlag' :: Int -> Word8 -> Word8
disableNthFlag' n p = p .&. (complement (shift 1 n))

toggleNthFlag :: Int -> Emulator s s2 ()
toggleNthFlag n = modifyP (toggleNthFlag' n)

toggleNthFlag' :: Int -> Word8 -> Word8
toggleNthFlag' n p = p `xor` (shift 1 n)

getNthFlag :: Int -> Emulator s s2 Bool
getNthFlag n = do
    p <- readP
    return $ getNthFlag' n p

getNthFlag' :: Int -> Word8 -> Bool
getNthFlag' n p = testBit p n


enableCarryFlag = enableNthFlag 0
enableZeroFlag = enableNthFlag 1
enableInterruptDisable = enableNthFlag 2
enableDecimalModeFlag = enableNthFlag 3
enableBreakCommand = enableNthFlag 4
enableOverflowFlag = enableNthFlag 5
enableNegativeFlag = enableNthFlag 6

pushStack :: MemoryValue -> Emulator s s2 ()
pushStack b = do
    addr <- readS
    writeMemory addr b
    decS

popStack :: Emulator s s2 MemoryValue
popStack = do
    incS
    addr <- readS
    readMemory addr

-- TODO: implement stack operations